/**
 * Copyright (C) 2021 Thomas Weber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import './import-first';

import ReactDOM from 'react-dom';
import React from 'react';
import {setAppElement} from 'react-modal';

import Interface from './render-interface.jsx';
import appTarget from './app-target';

let vm;

const onVmInit = _vm => {
    vm = _vm;
};

const onProjectLoaded = () => {
    let autoStart = false;

    const urlParams = new URLSearchParams(location.search);

    if (urlParams.has('autoplay')) {
        autoStart = true;
    }

    if (urlParams.has('autostart')) {
        autoStart = true;
    }

    if (urlParams.has('start')) {
        const startValue = urlParams.get('start');
        const startValidStrings = [
            'yes',
            'true',
            '1',
            'auto'
        ];
        if (startValidStrings.includes(startValue)) {
            autoStart = true;
        }
    }
    if (autoStart) {
        vm.start();
        vm.greenFlag();
    }

};

setAppElement(appTarget);
ReactDOM.render(<Interface
    isPlayerOnly
    isFullScreen
    onVmInit={onVmInit}
    onProjectLoaded={onProjectLoaded}
/>, appTarget);
