import React from 'react';
import {connect} from 'react-redux';
import GUI from '../containers/gui.jsx';
import adacraft from '../adacraft';

import {
    getIsShowingWithId
} from '../reducers/project-state';

const searchParams = new URLSearchParams(location.search);
const cloudHost = searchParams.get('cloud_host') || 'wss://clouddata.turbowarp.org';

setTimeout(() => {
    adacraft.enableProjectSaving();
}, 0);

const RenderGUI = props => (
    <GUI
        cloudHost={cloudHost}
        canSave={props.canSave}
        basePath={process.env.ROOT}
        canEditTitle
        {...props}
    />
);

const mapStateToProps = state => {
    const loadingState = state.scratchGui.projectState.loadingState;
    return {
        canSave: state.adacraft.canSave,
        // Only show the "see project oage" button for projects with an ID. I.e.
        // when it's an actual project that is opened and not the default one
        // for anonymous editing.
        enableCommunity: getIsShowingWithId(loadingState),
    };
};

export default connect(mapStateToProps)(RenderGUI);
