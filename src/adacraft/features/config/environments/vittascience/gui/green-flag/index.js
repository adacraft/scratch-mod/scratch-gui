import styles from './green-flag.css';
import icon from './icon--green-flag.svg'

export default {
    styles,
    icon
}