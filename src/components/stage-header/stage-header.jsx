import classNames from 'classnames';
import {defineMessages, injectIntl, intlShape} from 'react-intl';
import PropTypes from 'prop-types';
import React from 'react';
import {connect} from 'react-redux';
import VM from 'scratch-vm';

import Box from '../box/box.jsx';
import Button from '../button/button.jsx';
import Controls from '../../containers/controls.jsx';
import {getStageDimensions} from '../../lib/screen-utils';
import {STAGE_SIZE_MODES} from '../../lib/layout-constants';

import defaultFullScreenIcon from './icon--fullscreen.svg';

import largeStageIcon from './icon--large-stage.svg';
import smallStageIcon from './icon--small-stage.svg';

import defaultUnFullScreenIcon from './icon--unfullscreen.svg';

import browserFullScreenIcon from './icon--browser-fullscreen.svg';

import defaultStyles from './stage-header.css';

const defaultIcons = {
    fullscreen: defaultFullScreenIcon,
    unfullscreen: defaultUnFullScreenIcon
}
const defaultConfig = {
    icons: defaultIcons,
    styles: defaultStyles
}

// [adacraft] The layout can be configured based on the running environment.
let config = adacraft.features.config.gui.stageHeader || defaultConfig;

const { hideStageControls } = adacraft.features.decisions

import FullscreenAPI from '../../lib/tw-fullscreen-api';

import adacraft from '../../adacraft/index.js';

const messages = defineMessages({
    largeStageSizeMessage: {
        defaultMessage: 'Switch to large stage',
        description: 'Button to change stage size to large',
        id: 'gui.stageHeader.stageSizeLarge'
    },
    smallStageSizeMessage: {
        defaultMessage: 'Switch to small stage',
        description: 'Button to change stage size to small',
        id: 'gui.stageHeader.stageSizeSmall'
    },
    fullStageSizeMessage: {
        defaultMessage: 'Enter full screen mode',
        description: 'Button to change stage size to full screen',
        id: 'gui.stageHeader.stageSizeFull'
    },
    unFullStageSizeMessage: {
        defaultMessage: 'Exit full screen mode',
        description: 'Button to get out of full screen mode',
        id: 'gui.stageHeader.stageSizeUnFull'
    },
    fullscreenControl: {
        defaultMessage: 'Full Screen Control',
        description: 'Button to enter/exit full screen mode',
        id: 'gui.stageHeader.fullscreenControl'
    }
});

const StageHeaderComponent = function (props) {
    const {
        isFullScreen,
        isPlayerOnly,
        onKeyPress,
        onSetStageLarge,
        onSetStageSmall,
        onSetStageFull,
        onSetStageUnFull,
        isEmbedded,
        stageSizeMode,
        vm
    } = props;

    let header = null;

    if (isFullScreen || isEmbedded) {
        const stageDimensions = getStageDimensions(null, true);
        const dontShowFullscreenControl = (
            !adacraft.fullscreenExitIsAllowed(props)
            || (
                isEmbedded
                && !FullscreenAPI.available()
            )
        )
        const fullScreenControl = dontShowFullscreenControl ? (
            null
        ) : isFullScreen ? (
            <Button
                className={config.styles.stageButton}
                onClick={onSetStageUnFull}
                onKeyPress={onKeyPress}
            >
                <img
                    alt={props.intl.formatMessage(messages.unFullStageSizeMessage)}
                    className={config.styles.stageButtonIcon}
                    draggable={false}
                    src={config.icons.unfullscreen}
                    title={props.intl.formatMessage(messages.fullscreenControl)}
                />
            </Button>
            
        ) : (
            <Button
                className={config.styles.stageButton}
                onClick={onSetStageFull}
            >
                <img
                    alt={props.intl.formatMessage(messages.fullStageSizeMessage)}
                    className={config.styles.stageButtonIcon}
                    draggable={false}
                    src={config.icons.fullscreen}
                    title={props.intl.formatMessage(messages.fullscreenControl)}
                />
            </Button>
        );

        // [adacraft] Display controls (play/start bar, fullscreen button...)
        // based on URL query parameter named "control".
        let showMainControls = true;
        let showBrowserFullscreenControl = adacraft.features.decisions.showBrowserFullscreenControl;
        const urlParams = new URLSearchParams(location.search);
        if (urlParams.has('controls')) {
            const controlsValue = urlParams.get('controls');
            if (controlsValue === 'none') {
                showMainControls = false;
                showBrowserFullscreenControl = false;
            } else if (controlsValue === 'all') {
                showMainControls = true;
                showBrowserFullscreenControl = true;
            } else {
                showMainControls = false;
                showBrowserFullscreenControl = false;
                if (controlsValue.split(',').includes('main')) {
                    showMainControls = true;
                }
                if (controlsValue.split(',').includes('fullscreen')) {
                    showBrowserFullscreenControl = true;
                }
            }
        }
        
        // [adacraft] Add a button to enter/exit browser fullscreen (which is
        // not the same as Scratch fullscreen).
        const browserFullScreenControl = isFullScreen && showBrowserFullscreenControl ? (
            <Button
                className={config.styles.stageButton}
                onClick={() => adacraft.requestBrowserFullscreenSwitch()}
                onKeyPress={onKeyPress}
                style={{
                    borderWidth: 0,
                    backgroundColor: '#ffffff88',
                    // [adacraft] The browser fullscreen button is placed on the
                    // bottom right to be clearly separated from the Scratch
                    // fullscreen button (if any). Also it reminds the position
                    // in video players.
                    position: 'fixed',
                    bottom: '5px',
                    right: '5px'
                }}
            >
                <img
                    alt="Enter or exit browser fullscreen"
                    className={config.styles.stageButtonIcon}
                    draggable={false}
                    src={browserFullScreenIcon}
                    title="Enter or exit browser fullscreen"
                />
            </Button>
            
        ) : (
            null
        );

        header = (
            <Box
                className={classNames(config.styles.stageHeaderWrapperOverlay, {
                    [config.styles.embedded]: isEmbedded
                })}
            >
                <Box
                    className={config.styles.stageMenuWrapper}
                    style={{width: stageDimensions.width}}
                >
                    {showMainControls ? (<Controls vm={vm} />) : []}
                    {fullScreenControl}
                    {browserFullScreenControl}

                </Box>
            </Box>
        );
    } else {
        const stageControls =
            hideStageControls || isPlayerOnly ? (
                []
            ) : (
                <div className={config.styles.stageSizeToggleGroup}>
                    <div>
                        <Button
                            className={classNames(
                                config.styles.stageButton,
                                config.styles.stageButtonFirst,
                                (stageSizeMode === STAGE_SIZE_MODES.small) ? null : config.styles.stageButtonToggledOff
                            )}
                            onClick={onSetStageSmall}
                        >
                            <img
                                alt={props.intl.formatMessage(messages.smallStageSizeMessage)}
                                className={config.styles.stageButtonIcon}
                                draggable={false}
                                src={smallStageIcon}
                            />
                        </Button>
                    </div>
                    <div>
                        <Button
                            className={classNames(
                                config.styles.stageButton,
                                config.styles.stageButtonLast,
                                (stageSizeMode === STAGE_SIZE_MODES.large) ? null : config.styles.stageButtonToggledOff
                            )}
                            onClick={onSetStageLarge}
                        >
                            <img
                                alt={props.intl.formatMessage(messages.largeStageSizeMessage)}
                                className={config.styles.stageButtonIcon}
                                draggable={false}
                                src={largeStageIcon}
                            />
                        </Button>
                    </div>
                </div>
            );
        header = (
            <Box className={config.styles.stageHeaderWrapper}>
                <Box className={config.styles.stageMenuWrapper}>
                    <Controls vm={vm} />
                    <div className={config.styles.stageSizeRow}>
                        {stageControls}
                        <div>
                            <Button
                                className={config.styles.stageButton}
                                onClick={onSetStageFull}
                            >
                                <img
                                    alt={props.intl.formatMessage(messages.fullStageSizeMessage)}
                                    className={config.styles.stageButtonIcon}
                                    draggable={false}
                                    src={config.icons.fullscreen}
                                    title={props.intl.formatMessage(messages.fullscreenControl)}
                                />
                            </Button>
                        </div>
                    </div>
                </Box>
            </Box>
        );
    }

    return header;
};

const mapStateToProps = state => ({
    // This is the button's mode, as opposed to the actual current state
    stageSizeMode: state.scratchGui.stageSize.stageSize
});

StageHeaderComponent.propTypes = {
    intl: intlShape,
    isFullScreen: PropTypes.bool.isRequired,
    isPlayerOnly: PropTypes.bool.isRequired,
    onKeyPress: PropTypes.func.isRequired,
    onSetStageFull: PropTypes.func.isRequired,
    onSetStageLarge: PropTypes.func.isRequired,
    onSetStageSmall: PropTypes.func.isRequired,
    onSetStageUnFull: PropTypes.func.isRequired,
    isEmbedded: PropTypes.bool.isRequired,
    stageSizeMode: PropTypes.oneOf(Object.keys(STAGE_SIZE_MODES)),
    vm: PropTypes.instanceOf(VM).isRequired
};

StageHeaderComponent.defaultProps = {
    stageSizeMode: STAGE_SIZE_MODES.large
};

export default injectIntl(connect(
    mapStateToProps
)(StageHeaderComponent));
