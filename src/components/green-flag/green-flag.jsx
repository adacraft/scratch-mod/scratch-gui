import classNames from 'classnames';
import PropTypes from 'prop-types';
import React from 'react';

import adacraft from '../../adacraft';

import defaultIcon from './icon--green-flag.svg';
import defaultStyles from './green-flag.css';
const defaultConfig = {
    icon: defaultIcon,
    styles: defaultStyles
}

// [adacraft] The layout can be configured based on the running environment.
let config = adacraft.features.config.gui.greenFlag || defaultConfig;

const GreenFlagComponent = function (props) {
    const {
        active,
        className,
        onClick,
        title,
        ...componentProps
    } = props;
    return (
        <img
            className={classNames(
                className,
                config.styles.greenFlag,
                {
                    [config.styles.isActive]: active
                }
            )}
            draggable={false}
            src={config.icon}
            title={title}
            onClick={onClick}
            // tw: also fire click when opening context menu (right click on all systems and alt+click on chromebooks)
            onContextMenu={onClick}
            {...componentProps}
        />
    );
};
GreenFlagComponent.propTypes = {
    active: PropTypes.bool,
    className: PropTypes.string,
    onClick: PropTypes.func.isRequired,
    title: PropTypes.string
};
GreenFlagComponent.defaultProps = {
    active: false,
    title: 'Go'
};
export default GreenFlagComponent;
