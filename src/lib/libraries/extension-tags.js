import messages from './tag-messages.js';
export default [
    {tag: 'art', intlLabel: messages.art},
    {tag: 'ai', intlLabel: messages.ai},
    {tag: 'hardware', intlLabel: messages.hardware},
    {tag: 'network', intlLabel: messages.network},
    {tag: 'scratch', intlLabel: messages.scratch},
    {tag: 'others', intlLabel: messages.others}
];
