export default {
    // Disable Scratch's analytics
    event () {},
    // Disable TurboWarp's analytics
    twEvent (name) {
    }
};
