import adacraft from '../../adacraft';
const { costume } = adacraft.features.config.defaultProject;

import projectData from './project-data';

/* eslint-disable import/no-unresolved */
import backdrop from '!raw-loader!./cd21514d0531fdffb22204e0ec5ed84a.svg';

/* eslint-enable import/no-unresolved */
import {TextEncoder} from '../tw-text-encoder';

const defaultProject = translator => {
    const encoder = new TextEncoder();

    const projectJson = projectData(translator);
    return [{
        id: 0,
        assetType: 'Project',
        dataFormat: 'JSON',
        data: JSON.stringify(projectJson)
    }, {
        id: 'cd21514d0531fdffb22204e0ec5ed84a',
        assetType: 'ImageVector',
        dataFormat: 'SVG',
        data: encoder.encode(backdrop)
    }, {
        id: costume.projectData.assetId,
        assetType: costume.assetType,
        dataFormat: costume.projectData.dataFormat.toUpperCase(),
        data: encoder.encode(costume.assetContent)
    }];
};

export default defaultProject;
